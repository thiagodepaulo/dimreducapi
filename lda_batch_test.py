from preprocessor import Preprocessor
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier

from models import load_dataset, score, score_many, plot_measure


def train_lda_batch(topics, scoring, kmeans_k, datasets):

    preproc = Preprocessor(remove_html=True)

    pre = [('pre', preproc)]
    bow = pre + [('vect', CountVectorizer())]


    param_grid = [
        {
            'reduce_dim__n_components': topics,
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]

    pipe = Pipeline(bow + [
        ('reduce_dim', LatentDirichletAllocation(learning_method='batch')),
        ('classify', KNeighborsClassifier())])

    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)

    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/scikit-lda_batch.csv')

    for corpus in datasets.keys():
        plot_measure(df_scores, corpus, 'SciKit-Learn LDA batch', 'mean_test_f1_macro', 'Mean F1-Macro')

if __name__ == '__main__':
    train_lda_batch()