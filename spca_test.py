from preprocessor import Preprocessor
from sklearn.decomposition import SparsePCA
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from models import DenseTransformer
from models import load_dataset, score, score_many, plot_measure


def train_spca(topics, scoring, kmeans_k, datasets):


    preproc = Preprocessor(remove_html=True)

    pre = [('pre', preproc)]
    bow = pre + [('vect', CountVectorizer())]
    tfidf = bow + [('tfidf', TfidfTransformer()), ('to_dense', DenseTransformer())]


    param_grid = [
        {
            'reduce_dim': [SparsePCA()],
            'reduce_dim__n_components': topics,
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]

    pipe = Pipeline(tfidf + [
        ('reduce_dim', SparsePCA()),
        ('classify', KNeighborsClassifier())])

    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)

    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/spca.csv')

    for corpus in datasets.keys():
        plot_measure(df_scores, corpus, 'Scikit-Learn Sparse PCA', 'mean_test_f1_macro', 'Mean F1-Macro')

if __name__ == '__main__':
    train_spca()