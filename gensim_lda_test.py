from preprocessor import Preprocessor
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from models import GSLdaTransformer
from models import load_dataset, score, score_many, plot_measure


def train_gensim_lda(topics, scoring, kmeans_k, datasets):

    preproc = Preprocessor(remove_html=True)

    pre = [('pre', preproc)]
    bow = pre + [('vect', CountVectorizer())]

    param_grid = [
        {
            'reduce_dim': [GSLdaTransformer()],
            'reduce_dim__num_topics': topics,
            # 'reduce_dim__passes': [20],
            # 'reduce_dim__iterations': [400],
            # 'reduce_dim__eval_every': [None],
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]

    pipe = Pipeline(bow + [
        ('reduce_dim', GSLdaTransformer()),
        ('classify', KNeighborsClassifier())])

    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)

    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/gensim_lda.csv')

    for corpus in datasets.keys():
        plot_measure(df_scores, corpus, 'Gensim LDA', 'mean_test_f1_macro', 'Mean F1-Macro',
                     n_topics_col='param_reduce_dim__num_topics')

if __name__ == '__main__':
    train_gensim_lda()