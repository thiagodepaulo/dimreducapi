
DR_METHOD_GENSIM_LDA = 'gensim_lda'
DR_METHOD_SCKIT_LDA = 'sckit_lda'
DR_METHOD_GENSIM_LSI = 'gensim_lsi'
DR_METHOD_SCKIT_LSI = 'sckit_lsi'
DR_METHOD_W2V = 'w2v'
DR_METHOD_R_LSI = 'r_lsi'
DR_METHOD_R_LDA = 'r_lda'

DR_NOT = 'dr_not' # do not apply dimensionality reduction!
