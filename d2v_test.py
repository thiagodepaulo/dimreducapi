import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from models import WordTokenizerTransformer
from gensim.sklearn_api import D2VTransformer
from models import load_dataset, score, score_many, plot_measure

def train_d2v(topics, scoring, kmeans_k, datasets):

    wt = WordTokenizerTransformer()

    preproc = [('pre', wt)]


    param_grid = [
        {
            'reduce_dim': [D2VTransformer()],
            'reduce_dim__size': topics,
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]

    pipe = Pipeline(preproc + [
        ('reduce_dim', D2VTransformer()),
        ('classify', KNeighborsClassifier())])

    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)


    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/doc2vec.csv')

    for corpus in datasets.keys():
        plot_measure(df_scores, corpus, 'Gensim doc2vec', 'mean_test_f1_macro', 'Mean F1-Macro',
                     n_topics_col = 'param_reduce_dim__size')

if __name__ == '__main__':
    train_d2v()