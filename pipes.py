import pandas as pd
import numpy as np
from preprocessor import Preprocessor
from sklearn.decomposition import NMF, LatentDirichletAllocation, TruncatedSVD, SparsePCA
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score, cross_validate
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC
from sklearn.ensemble import (ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier)
from models import GSFastTextTransformer, DenseTransformer
import os

scoring = [
        'accuracy',
        'f1_micro',
        'f1_macro',
        'f1_weighted'
    ]

topics = [10, 50, 100, 150, 200, 250, 300]



def score(pipe, X, Y, clf, vec, dim):

    scores_file = 'results/%s_%s_%s.csv' % (vec, dim, clf)
    if os.path.exists(scores_file):
        print('Scores loaded:', scores_file)
        return pd.read_csv(scores_file)

    scores = cross_validate(pipe, X, Y, cv=3, scoring=scoring, return_train_score=False)
    scores = [(metric, np.mean(mcv)) for (metric, mcv) in scores.items()]
    scores = pd.DataFrame(scores, columns=['metric', 'value'])
    scores['vec'] = vec
    scores['clf'] = clf
    scores['dim'] = dim
    scores_file = 'results/%s_%s_%s.csv' % (vec, dim, clf)
    scores.to_csv(scores_file)
    print('Scores saved:', scores_file)
    return scores

df = pd.read_csv('data/SW/SyskillWeber.csv')


preproc = Preprocessor()
# p_corpus = preproc.transform(df['text'].values)
X = df['text'].values
Y = df['class'].values
class_count = len(df['class'].value_counts())

pre = [('pre', preproc)]
bow = pre + [('vect', CountVectorizer())]
tfidf = bow + [('tfidf', TfidfTransformer())]
svd = {nc: tfidf + [('svd', TruncatedSVD(n_components=nc))] for nc in topics}
spca = {nc: tfidf + [('to_dense', DenseTransformer()), ('spca', SparsePCA(n_components=nc))] for nc in topics}
nmf = {nc: tfidf + [('nmf', NMF(n_components=nc))] for nc in topics}
ft = {nc: [('ft', GSFastTextTransformer({"size": nc}))] for nc in topics}

p_bow_lsvc = Pipeline(bow + [('clf', LinearSVC())])
p_tfidf_lsvc = Pipeline(tfidf + [('clf', LinearSVC())])

p_svd_lsvc = {nc: Pipeline(svd[nc] + [('clf', LinearSVC())]) for nc in topics}
p_spca_lsvc = {nc: Pipeline(spca[nc] + [('clf', LinearSVC())]) for nc in topics}
p_nmf_lsvc = {nc: Pipeline(nmf[nc] + [('clf', LinearSVC())]) for nc in topics}
p_ft_lsvc = {nc: Pipeline(ft[nc] + [('clf', LinearSVC())]) for nc in topics}
p_ft_knn = {nc: Pipeline(ft[nc] + [('clf', KNeighborsClassifier(n_neighbors=class_count))]) for nc in topics}

# p_nmf_lsvc = {nc: Pipeline([
#     ('vect', CountVectorizer()),
#     ('tfidf', TfidfTransformer()),
#     ('nmf', NMF(n_components=nc)),
#     ('clf', LinearSVC())
#     ]) for nc in topics}


scores = []
scores.append(score(p_tfidf_lsvc, X, Y, 'lsvc', 'tfidf', 'all'))
scores.append(score(p_bow_lsvc, X, Y, 'lsvc', 'bow', 'all'))
scores += [score(p_svd_lsvc[nc], X, Y, 'lsvc', 'svd', nc) for nc in topics]
scores += [score(p_spca_lsvc[nc], X, Y, 'lsvc', 'spca', nc) for nc in topics]
# scores += [score(p_nmf_lsvc[nc], X, Y, 'lsvc', 'nmf', nc) for nc in topics]
scores += [score(p_ft_lsvc[nc], X, Y, 'lsvc', 'ft', nc) for nc in topics]
scores += [score(p_ft_knn[nc], X, Y, 'knn', 'ft', nc) for nc in topics]

df_all = pd.concat(scores, ignore_index=True)
df_all.to_csv('results/all.csv')
# print(df_all)
# df_all = df_all.pivot(index='vec', columns='dim')
print(df_all)
