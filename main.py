
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from preprocessor import Preprocessor
from util import Loader
import pandas as pd
import logging
import sys
from sklearn.metrics import classification_report
from r_lsi import RLSI
from sklearn.ensemble import (ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier)
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from estimater_select import EstimatorSelectionHelper
from optparse import OptionParser
import json
from models import get_model

# apply dimensionality reduction
def red_dim(M, dr_method, params):
    DR = get_model(dr_method, params)
    print(M.shape[1], type(M.shape[1]))
    return DR.fit_transform(M)


def evaluator(d, M):
    models1 = {
        'ExtraTreesClassifier': ExtraTreesClassifier(),
        'RandomForestClassifier': RandomForestClassifier(),
        'AdaBoostClassifier': AdaBoostClassifier(),
        'GradientBoostingClassifier': GradientBoostingClassifier(),
        'SVC': SVC(),
        'MultinomialNB': MultinomialNB(),
        'KNN': KNeighborsClassifier()
    }

    params1 = {
        'ExtraTreesClassifier': {'n_estimators': [16, 32]},
        'RandomForestClassifier': {'n_estimators': [16, 32]},
        'AdaBoostClassifier': {'n_estimators': [16, 32]},
        'GradientBoostingClassifier': {'n_estimators': [16, 32], 'learning_rate': [0.8, 1.0]},
        'SVC': [
            {'kernel': ['linear'], 'C': [1, 10]},
            {'kernel': ['rbf'], 'C': [1, 10], 'gamma': [0.001, 0.0001]},
        ],
        'MultinomialNB': {'alpha': [0.1, 0.01, 0.001]},
        'KNN': {'n_neighbors': [1,3,5,7], 'metric': 'cosine' }
    }

    scoring = {
        'accuracy',
        'f1_micro',
        'f1_macro',
        'f1_weighted'
    }

    helper1 = EstimatorSelectionHelper(models1, params1)
    helper1.fit(M, d['class_index'], scoring=scoring, n_jobs=-1, cv=10)

    # fix it, writ in a file
    return helper1.score_summary(sort_by='min_score')

if __name__ == '__main__':

    # main program parameters
    parser = OptionParser()
    usage = "usage: python %prog [options] args ..."
    description = """Description"""
    parser.add_option("-c", "--corpus", dest="corpus_file", help="input file dataset", default='/exp/datasets/docs_rotulados/SyskillWebert-Parsed')
    parser.add_option("-i", "--config", dest="config_file", help="configuration file")
    #parser.add_option("-m", "--min_df", dest="min_df", help="minimum word frequency", default=2)
    #parser.add_option("-k", dest="k", help="number of components (topics)", default=50)
    #parser.add_option("-p", "--param", dest="param_file", help="parameter file")
    #parser.add_option("-o", "--output", dest="output", help="output csv file", default='output.csv')
    #parser.add_option("-r", "--dim_reduction", dest="dr_method", help="output csv file", default='output.csv')
    (options, args) = parser.parse_args()
    dataset_file = options.corpus_file
    configs = json.load(open(options.config_file))
    min_df = int(configs['min_df'])
    K = int(configs['k'])
    output_file = configs['output']
    stem = bool(configs['stem'])

    ldr = ['dr_not', 'gensim_lda', 'sckit_lda', 'gensim_lsi', 'sckit_lsi', 'w2v', 'r_lsi', 'r_lda']
    dr_name = ldr[int(configs['dr'])]     #  dimensionality reduction method

    # load datasets
    l = Loader()
    d = l.from_files(dataset_file)

    # preprocessing
    # TO DO it better (refact it!)
    preproc = Preprocessor(stem=stem)
    preproc_corpus = preproc.transform(d['corpus'])
    cvect = CountVectorizer(min_df=min_df)
    M = cvect.fit_transform(preproc_corpus)
    tfidfcorpus = TfidfTransformer()
    M2 = tfidfcorpus.fit_transform(M)

    # dimensionality reduction
    Mred = red_dim(M2, dr_name, configs['params'])

    # evaluate by classification
    df = evaluator(d, Mred)
    df.to_csv(output_file)


