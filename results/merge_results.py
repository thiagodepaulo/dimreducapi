import pandas as pd
import glob as glob


excludes = ['svd_acm_20ng_trec_sw.csv', 'merged.csv']

csv_files = [f for f in glob.glob('*.csv') if f not in excludes]

print('Files being merged:')
for csv in csv_files: print(' - ', csv)

reduce_dim_algos = [csv.split('.')[0] for csv in csv_files]
dfs = [pd.read_csv(csv) for csv in csv_files]
for df, algo in zip(dfs, reduce_dim_algos):
    df['reduce_dim_algo'] = algo

# Models trainded with Gensim uses parameter 'n_topics' in pipeline.
# For our purposes, it is equivalent to 'n_components'.
# So, for dataframes with 'param_reduce_dim__num_topics' column,
# We rename it to 'param_reduce_dim__n_components'
dfs_ntopics = [df for df in dfs if 'param_reduce_dim__num_topics' in df.columns]
for df in dfs_ntopics:
    df.rename(index=str, columns={"param_reduce_dim__num_topics": "param_reduce_dim__n_components"}, inplace=True)

# Same must be done for word embeddings algo, that takes 'reduce_dim__size' instead of 'n_components'
dfs_nsize = [df for df in dfs if 'param_reduce_dim__size' in df.columns]
for df in dfs_nsize:
    df.rename(index=str, columns={"param_reduce_dim__size": "param_reduce_dim__n_components"}, inplace=True)



merged_df = pd.concat(dfs)

assert 'param_reduce_dim__num_topics' not in merged_df.columns
assert 'param_reduce_dim__size' not in merged_df.columns

print('Merged dataframe columns:')
for c in merged_df.columns: print(' - ', c)

print('Text corpus in merged dataframe')
for c in merged_df.corpus.unique(): print(' - ', c)

print('Total lines in merged_dataframe: ', len(merged_df))

print('Saving merged dataframe to file "merged.csv"')
merged_df.to_csv("merged.csv")