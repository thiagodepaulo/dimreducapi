import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from preprocessor import Preprocessor
from util import Loader

from sklearn.decomposition import NMF, LatentDirichletAllocation, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.decomposition import TruncatedSVD
from sklearn.base import TransformerMixin, BaseEstimator
from gensim.sklearn_api import LsiTransformer, RpTransformer
from gensim.sklearn_api import LdaTransformer
from gensim.sklearn_api import D2VTransformer
from gensim.models.fasttext import FastText
from gensim.models import doc2vec
from gensim.models.wrappers import LdaMallet

from preprocessor import remove_html_tags
from nltk.corpus import stopwords
from gensim import matutils

stopwords = stopwords.words('english')

models = {
    'none': lambda p: None,
    'sckit_nmf': lambda p: get_NMF(p),
    'sckit_lda': lambda p: get_LDA(p),
    'sckit_lsi': lambda p: get_LSI(p),
    'gensim_lda': lambda p: get_Gensim_LDA(p),
    'gensim_lsi': lambda p: get_Gensim_LSI(p),
    'gensim_d2v': lambda p: get_Gensim_D2V(p),
    'gensim_lda_mallet': lambda p: get_Gensim_LDA_Mallet(p),
    'gensim_fasttext': lambda  p: get_Gensim_FastText(p)
}


def get_model(model_name, parms):
    mdl = models[model_name](parms)
    return mdl


def get_NMF(parms):
    #n_components=parms['n_components'], random_state=1, alpha=.1, l1_ratio=.5
    #n_components=n_components, random_state=1, beta_loss='kullback-leibler', solver='mu', max_iter=1000, alpha=.1,
    #      l1_ratio=.5
    nmf = NMF(**parms)
    return nmf

def get_LDA(parms):
    #n_components = n_components, max_iter = 5,
    #learning_method = 'online',
    #learning_offset = 50.,
    #random_state = 0
    lda = LatentDirichletAllocation(**parms)
    return lda

def get_LSI(parms):
    lsi = TruncatedSVD(**parms)
    return lsi

def get_Gensim_LDA(parms):
    glda = GSLdaTransformer(parms)
    return glda

def get_Gensim_LSI(parms):
    glsi = GSLsiTransformer(parms)
    return glsi

def get_Gensim_D2V(parms):
    d2v = D2VTransformer(**parms)
    return d2v

def get_Gensim_LDA_Mallet(parms):
    pass

def get_Gensim_FastText(params):
    ftt = GSFastTextTransformer()
    ftt.set_params(**params)
    return ftt

class GSLsiTransformer(TransformerMixin, BaseEstimator):
    # def __init__(self, parms):
    #     self.lsi = LsiTransformer(**parms)

    def fit(self, X, y=None):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        self.lsi.fit(gs_X)
        return self

    def transform(self, X, *_):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        return self.lsi.transform(gs_X)

    def set_params(self, **params):
        self.lsi = LsiTransformer(**params)

class GSRPTransformer(TransformerMixin, BaseEstimator):
    # def __init__(self, parms):
    #     self.lsi = LsiTransformer(**parms)

    def fit(self, X, y=None):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        self.rp.fit(gs_X)
        return self

    def transform(self, X, *_):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        return self.rp.transform(gs_X)

    def set_params(self, **params):
        self.rp = RpTransformer(**params)


class GSLdaTransformer(TransformerMixin, BaseEstimator):
    def __init__(self, n_topics=100, iterations=400, passes=20, eval_every=None):
        self.n_topics = n_topics
        self.iterations = iterations
        self.passes = passes
        self.eval_every = eval_every

    def fit(self, X, y=None):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        self.lda.fit(gs_X)
        return self

    def transform(self, X, *_):
        gs_X = [matutils.scipy2sparse(v) for v in X]
        return self.lda.transform(gs_X)

    def set_params(self, **params):
        self.lda = LdaTransformer(**params)
        if 'iterations' not in params:
            self.lda.iterations = self.iterations
        if 'passes' not in params:
            self.lda.passes = self.passes
        if 'eval_every' not in params:
            self.lda.eval_every = self.eval_every

class GSFastTextTransformer(TransformerMixin, BaseEstimator):
    def __init__(self):
        self.ft = FastText()

    def fit(self, X, y=None):
        # X_ = [t.lower().split() for t in X]
        self.ft.build_vocab(X)
        self.ft.train(X, total_examples=self.ft.corpus_count, epochs=self.ft.epochs)
        return self

    def transform(self, X, *_):
        # X_ = [[w for w in d if w in self.ft.wv.vocab] for d in X]
        vects = [self.ft[" ".join(v)] for v in X] #if v in self.ft.wv.vocab
        return vects

    def set_params(self, **params):
        self.ft = FastText(**params)


class DenseTransformer(TransformerMixin, BaseEstimator):

    def transform(self, X, y=None, **fit_params):
        return X.todense()

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self

class WordTokenizerTransformer(TransformerMixin, BaseEstimator):

    def transform(self, X, y=None, **fit_params):
        xt = [remove_html_tags(x).lower().split() for x in X]
        xt = [[w for w in x if x not in stopwords] for x in xt]
        return xt

    def fit_transform(self, X, y=None, **fit_params):
        self.fit(X, y, **fit_params)
        return self.transform(X)

    def fit(self, X, y=None, **fit_params):
        return self


def load_dataset(dataset_name, dataset_folder):
    if dataset_name == 'SyskillWeber':
        df = pd.read_csv('data/SyskillWeber.csv')
        X = df.text
        Y = df['class']
        return X, Y
    else:
        l = Loader()
        d = l.from_files(dataset_folder)
        X = d['corpus']
        Y = d['class_index']
        return X, Y


def score(grid_cv, dataset_name, dataset_folder):

    X, Y = load_dataset(dataset_name, dataset_folder)
    grid_cv.fit(X, Y)

    df = pd.DataFrame(grid_cv.cv_results_)
    df['corpus'] = dataset_name
    return df


def score_many(grid_cv, data_sets_dict):

    scores = [score(grid_cv, k, data_sets_dict[k]) for k in data_sets_dict.keys()]
    scores = pd.concat(scores)
    return scores


def plot_measure(df, corpus, reduce_dim_algo, measure, measure_label=None, n_topics_col = 'param_reduce_dim__n_components'):
    df = df[df.corpus == corpus]
    n_comp = df[n_topics_col].unique()
    n_knn_n = df.param_classify__n_neighbors.unique()
    n_groups = len(n_comp)

    df_measure = df.groupby([n_topics_col, 'param_classify__n_neighbors']).mean()
    df_measure = df_measure[measure]
    knn = {k: df_measure.xs(k, level=1) for k in n_knn_n}

    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.2

    opacity = 0.4

    colors = 'bgr'
    for i, k in enumerate(n_knn_n):
        ax.bar(index + i * bar_width, knn[k], bar_width, alpha=opacity, color=colors[i], label='k = %d' % k)

    measure_label = measure_label if measure_label else measure
    ax.set_xlabel('Number of components')
    ax.set_ylabel(measure_label)
    ax.set_title('%s %s for %s corpus' % (reduce_dim_algo, measure_label, corpus))
    ax.set_xticks(index + bar_width / 2)
    ax.set_xticklabels(n_comp)
    ax.legend(loc='center right')

    fig.tight_layout()
    fig.savefig('results/%s-%s.png' % (reduce_dim_algo, corpus))
    plt.close(fig)


def plot_corpus_measure(df, corpus, measure, measure_label):

    df_group = df.groupby(['reduce_dim_algo', 'corpus']).mean()
    df_group = df_group[measure] #mean_test_f1_micro
    corpus_f1 = {k : df_group.xs(k, level=1).sort_values() for k in df.corpus.unique()}

    fig, ax = plt.subplots()

    X = range(len(corpus_f1[corpus]))
    Y = corpus_f1[corpus].values
    ax.barh(X,Y)
    plt.yticks(X, corpus_f1[corpus].index)
    plt.ylabel('Dimensionality reduction technique')
    plt.xlabel('Classification %s' % measure_label)
    plt.title('Classification %s by dimensionality reduction technique for corpus %s' % (corpus, measure_label) )

    fig.savefig('results/%s-%s.png' % (corpus, measure))
    plt.close(fig)



if __name__ == '__main__':

    X, Y = load_dataset('SyskillWeber', 'data/SyskillWeber.csv')

    wordtok = WordTokenizerTransformer()
    simple_corpus = wordtok.transform(X)
    print(simple_corpus[0])

    preproc = Preprocessor()
    p_corpus = preproc.transform(X)

    cvect = CountVectorizer()
    bows = cvect.fit_transform(p_corpus)

    tfidf = TfidfTransformer()
    bows_tfidf = tfidf.fit_transform(bows)
    print(bows_tfidf[0])

    lsi = get_model('gensim_lsi', {'num_topics':  50})
    vects_lsi_gensim = lsi.fit_transform(bows_tfidf)
    print(vects_lsi_gensim[0])

    lda = get_model('gensim_lda', {'num_topics': 50})
    vects_lda_gensim = lda.fit_transform(bows)
    print(vects_lda_gensim[0])

    d2v = get_model('gensim_d2v', {'size': 50})
    docvects = d2v.fit_transform(simple_corpus)
    print(docvects[0])

    parms = {
        'n_components': 50,
        'random_state':  0,
        'learning_offset': 50,
        'learning_method': 'batch',
        'max_iter': 50
    }

    lda = get_model('sckit_lda', parms)
    vects_lda_sk = lda.fit_transform(bows)
    print(vects_lda_sk[0])

    ft = get_model('gensim_fasttext', {'size': 50})
    ft = GSFastTextTransformer()
    ft.set_params(size = 50)
    ft_vects = ft.fit_transform(simple_corpus)

    print(ft_vects[0])

    nmf = get_model('sckit_nmf', {'n_components': 50})
    nmf_vects = nmf.fit_transform(bows_tfidf)
    print(nmf_vects[0])

    #print(df.head(1))
