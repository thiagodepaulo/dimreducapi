

topics = [5, 10, 50, 100, 150, 200, 250, 300]
scoring = ['accuracy', 'f1_micro', 'f1_macro', 'f1_weighted']
kmeans_k = [3, 5, 7]

datasets = {
            'SyskillWeber': 'data/SyskillWebert-Parsed',
            'CSTR': 'data/CSTR',
            'Irish Economic Sentiment': 'data/Irish Economic Sentiment',
            'NSF': 'data/NSF',
            'Opinosis-Parsed': 'data/Opinosis-Parsed'
            }
