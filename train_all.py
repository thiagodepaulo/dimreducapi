
#Should do this for running in our linux environment
import matplotlib
matplotlib.use('Agg')

import bow_test
import d2v_test
import fasttext_test
import gensim_lda_test
import gensim_lsi_test
import gensim_rp_test
import grp_test
import lda_batch_test
import lda_test
import nmf_test
import spca_test
import srp_test
import svd_test
import tfidf_test

import sys, traceback


topics = [5, 10, 50, 100, 150, 200, 250, 300]
scoring = ['accuracy', 'f1_micro', 'f1_macro', 'f1_weighted']
kmeans_k = [3, 5, 7]

datasets = {
            'SyskillWeber': 'data/SyskillWebert-Parsed',
            'CSTR': 'data/CSTR',
            'Irish Economic Sentiment': 'data/Irish Economic Sentiment',
            'NSF': 'data/NSF',
            'Opinosis-Parsed': 'data/Opinosis-Parsed'
            }


trains = [bow_test.train_bow,
          d2v_test.train_d2v,
          fasttext_test.train_fasttext,
          gensim_lda_test.train_gensim_lda,
          gensim_lsi_test.train_gensim_lsi,
          gensim_rp_test.train_gensim_rp,
          grp_test.train_gaussian_rp,
          lda_batch_test.train_lda_batch,
          lda_test.train_lda_online,
          nmf_test.train_nmf,
          spca_test.train_spca,
          srp_test.train_sparse_random_projection,
          svd_test.train_svd,
          tfidf_test.train_tfidf]

for t in trains:
    print('Excecuting: ', t)
    try:
        t(topics, scoring, kmeans_k, datasets)
        print('Done!')
    except Exception as e:
        print("Exception occured in ", t)
        traceback.print_exc(file=sys.stdout)
        print(e)