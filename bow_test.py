from preprocessor import Preprocessor
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from models import load_dataset, score, score_many, plot_measure


def train_bow(topics, scoring, kmeans_k, datasets):

    preproc = Preprocessor(remove_html=True)

    pre = [('pre', preproc)]
    bow = pre + [('vect', CountVectorizer())]

    param_grid = [
        {
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]

    pipe = Pipeline(bow + [
        ('classify', KNeighborsClassifier())])

    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)

    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/bow.csv')

    # for corpus in datasets.keys():
    #     plot_measure(df_scores, corpus, 'Scikit-Learn SVD', 'mean_test_f1_macro', 'Mean F1-Macro')

if __name__ == '__main__':
    train_bow()