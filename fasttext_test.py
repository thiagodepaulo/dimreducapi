from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from models import WordTokenizerTransformer, GSFastTextTransformer
from models import load_dataset, score, score_many, plot_measure

def train_fasttext(topics, scoring, kmeans_k, datasets):

    wt = WordTokenizerTransformer()

    preproc = [('pre', wt)]

    pipe = Pipeline(preproc + [
        ('reduce_dim', GSFastTextTransformer()),
        ('classify', KNeighborsClassifier())])

    param_grid = [
        {
            'reduce_dim': [GSFastTextTransformer()],
            'reduce_dim__size': topics,
            # 'reduce_dim__model': ['cbow', 'skipgram'],
            'classify__n_neighbors': kmeans_k,
            'classify__metric': ['cosine'],
        },
    ]


    grid = GridSearchCV(pipe,
                         param_grid,
                         cv=3, verbose=True,
                         scoring=scoring,
                         refit=False, return_train_score=False)

    df_scores = score_many(grid, datasets)
    df_scores.to_csv('results/fasttext.csv')

    for corpus in datasets.keys():
        plot_measure(df_scores, corpus, 'Gensim FastText', 'mean_test_f1_macro', 'Mean F1-Macro',
                     n_topics_col = 'param_reduce_dim__size')


if __name__ == '__main__':
    train_fasttext()